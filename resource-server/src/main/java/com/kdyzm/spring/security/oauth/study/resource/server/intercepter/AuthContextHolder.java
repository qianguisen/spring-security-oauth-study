package com.kdyzm.spring.security.oauth.study.resource.server.intercepter;

/**
 * @author kdyzm
 */

import com.alibaba.ttl.TransmittableThreadLocal;
import com.kdyzm.spring.security.oauth.study.resource.server.dto.UserDetailsExpand;

public class AuthContextHolder {
    private TransmittableThreadLocal threadLocal = new TransmittableThreadLocal();
    private static final AuthContextHolder instance = new AuthContextHolder();

    private AuthContextHolder() {
    }

    public static AuthContextHolder getInstance() {
        return instance;
    }

    public void setContext(UserDetailsExpand t) {
        this.threadLocal.set(t);
    }

    public UserDetailsExpand getContext() {
        return (UserDetailsExpand)this.threadLocal.get();
    }

    public void clear() {
        this.threadLocal.remove();
    }
}
